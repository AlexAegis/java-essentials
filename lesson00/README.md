## Eszközök

#### GitHub
https://github.com/
Először is regisztráljunk. Ezen az oldalon fogjuk hostolni a repository-nkat 
ami gyakorlatilag egy mappa amit a Git verziókezelő fog nekünk 
manageelni, hogy egyszerre többen is dolgozhassunk ugyanazon a projecten.

Ennek a repository-nak az elérési címe: https://github.com/AlexAegis/java-essentials.git

A végéről a ".git" elhagyható.

A GitHub automatikusan megjelenít minden "README.md" nevű markdown filet ezért az összes
szöveges leírás ilyen nevű fileokban lesz.

#### Git
https://git-scm.com/downloads

Szükségünk lesz magára a Git verziókezelőre is. Ez a program tartja szinkronban 
a különböző repository-kat úgy mint a te local repódat (a mappa amibe dolgozol)
és a remote repót (amit a GitHub-on hostolunk)
Telepítés után a "git.exe" automatikusan hozzákerül a Windows "PATH" környezeti
változói közé így a command lineból bármikor elérjük a "git" paranccsal.
A git az összes információját egy rejtett ".git" mappában tárolja a repository
gyökerében, itt általában nem lesz dolgunk de például hook-okat itt tudunk 
bekonfigurálni (a repohoz érkezett parancsokhoz akár egész script-eket, további
automatikusan reforduló parancsokat rendelhetünk)
Emelett a mappa gyökerében van egy ".gitignore" file is ami reguláris kifejezéseket
tartalmaz (a #-val jelölt sorok kommentek) és jelzi az indexelőnek, hogy mely 
fájlokat, mappákat ne vegye figyelembe indexelés során. Java esetében csak a 
forráskódokat (*.java) akarjuk megtartani, az ezekből készült bájtkódot (*.class)
fájlokat már nem ezért a gitignore fileunk mindenképpen tartalmazni fog egy 
"*.class" sort.

Fő parancsok: 
- git clone "repo elérési címe" 
	lemásolja a repository-t a parancs kiadásának helyére
- git add "fájlok nevei"
	hozzáadja a megadott fájl/fájlokat az index-hez
	"git add *" mindent hozzáad az aktuális mappában
- git status
	lekérdezi az index aktuális állapotát
- git commit -m "üzenet"
	hozzáadja az indexelt változtatásokat a local repository-dhoz, az üzenet kötelező
	ha nem adunk meg argumentumként üzenetet akkor automatikusan megnyílik egy vim 
	amiben ezt meg kell tennünk
	esc, :wq, enter
- git pull
	frissíti a local repository-dat a remote-repository-ban történt változtatások alapján
- git push
	a local repository-dban történt változtatásokat feltölti a remote repositoryba
	csak friss local repository-t tudunk pusholni. (kivétel a force push)

Addig amíg a repository-n belül vagy, a parancsokat bárhonnan kiadhatod, de a "git add" parancs az aktuális mappában lévő fájlokat látja

Ezen kívül még rengeteg mindent lehet csinálni a Git-el de mi egyelőre csak arra fogjuk 
használni, hogy egy helyre tudjunk dolgozni, lássuk egymás kódját/változtatásait és 
azokat egyből ki is tudjuk próbálni, esetleg változtatni benne.

#### Sublime Text
https://www.sublimetext.com/3

Szükségünk lesz egy kényelmes szövegszerkesztőre is.

#### Java DK
http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

Szükségünk lesz természetesen a Java Developement Kit-re is mellyel fordíthatjuk
a ".java" file-ainat és futtathatjuk a belőlük képzett ".class" bájtkódot.
Sajnos a telepítő csak a futtatásra való "java.exe" állományt teszi a "PATH"
környezeti változók közé, a "javac.exe"-t nem. Ez a Java Compiler állománya, ő
végzi a fordítást, a különböző fejlesztői környezetek (IDE) kezelik ezt a folyamatot helyettünk
de mivel mi kézzel akarunk fordítani így szükségünk lesz arra, hogy a compilert gyorsan
elérjük.

(A java programok futtatására önmagában csak a JRE, Java Runtime Environmentre 
van szükség de ezt tartalmazza a JDK)

Jobb klikk start menü -> rendszer -> speciális rendszerbeállítások -> környezeti változók
Az alsó listából keressük meg a "Path" nevűt, duplaklikk és vegyünk fel egy új sort és
állítsuk be neki a "C:\Program Files\Java\jdk1.8.0_121\bin" értéket. (A jdk alapértelmezetten
ide települ)

#### Maven (later)
https://github.com/AlexAegis/maven-repository

A maven telepítéséről már részletesebben itt írtam, de egyelőre vele nem fogunk foglalkozni.
