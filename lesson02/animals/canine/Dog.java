package animals.canine;

import animals.Animal;

public class Dog extends Animal {

	public Dog() {

	}

	public Dog(String name) {
		this.name = name;
	}

	public void bark() {
		System.out.println(name);
	}

	@Override
	public void run() {
		System.out.println("Dog runs");
	}

	// A Dog objektumok ezt az equals metódust használják 
	// nem pedig azt ami az Animals osztályban van 
	// mert Overrideolva van
	@Override
	public boolean equals(Object obj) {
		
		if(obj == null) {
			return false;
		}

		//Típusellenőrzés
		if(!(obj.getClass() == Dog.class)) {
			return false;
		}

		Dog dog = (Dog) obj;

		return this.name == dog.name;
	}
}