class Number {
	
	public static void main(String[] args) {
		int numberTen = 10;
		int numberEleven = 11;

		if(numberTen % 2 == 0) {
			System.out.println("Páros");
	    } else {
			System.out.println("Páratlan");	
		}

	    if(numberEleven % 2 == 0) {
			System.out.println("Páros");
	    } else {
			System.out.println("Páratlan");	
		}
	}
}