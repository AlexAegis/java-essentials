package animals;

public class Animal {

	// Nullszerű értékkekkel inicializálódnak alapból
	// Az objektumok mindíg "null"-al a primitívek meg nullaszerű értékekkel
	// int 0; float 0f; double 0d; boolean false; stb	
	public String name/*= null*/;
	// A protected jelen van az összes innen örökölt 
	// osztályban és csak az osztályokon belül érhető el
	protected int age/*= 0*/;
	// A private mező/adattag csak ebben az osztályban látható
	private int id/*= 0*/;

	public Animal() {

	}

	public void run() {
		System.out.println("Animal runs");
	}

	// Ez itt egy Getter, csak visszaadja egy olyan 
	// mező/adattag értékét amihez alapból nem férnénk hozzá
	public int getAge() {
		return age;
	}

	@Override
	public boolean equals(Object obj) {
		// Ellenőrizzük, hogy egyáltalán kaptunk e objektumot
		if(obj == null) {
			return false;
		}
		// Elenőrizzük, hogy a kapott objektum olyan típusú e amire később Castolni akarjuk
		if(!(obj.getClass() == Animal.class)) {
			return false;
		}

		// Castoljuk az objektumot olyanra mint ez az osztály
		Animal animal = (Animal) obj;

		// Ténylegesen összehasonlítjuk a két objektum tulajdonságait
		return this.name == animal.name 
			&& this.age == animal.age;
	}

}
/* Ilyesmi dolgokat tartalmaz az Object osztály, ebből írjuk felül az equals metódust
public class Object {

	public String toString() {
		return "";
	}

	public boolean equals(Object obj) {
		return this == obj;
	}

	public int hashCode() {
		return 1;
	}

}*/