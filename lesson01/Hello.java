// Más osztály importálása
import animals.Animal;

// Osztály leírás, a neve a fájl nevével egyezik meg mindig
// és konvencionálisan mindig nagybetűvel kezdődik
class Hello {

	// Statikus adattagok / mezők
	// Kisbetűvel kezdődnek és a nevükben lévő
	// többi szót pedig nagybetűvel jelöljük, hogy
	// könnyebben olvasható legyen
	public static Animal allatA;
	public static Animal allatB;
	public static int number;

	// Program belépési pontja (main függvény)
	public static void main(String[] args) {

		// Új objektum létrehozása és referenciához csatolása
		// A new kulcsszó meghívja az Animal osztály Animal()
		// függvényét amit kontstruktornak hívunk
		allatA = new Animal();
		allatB = new Animal();

		// Objektumon történő metódushívás
		// A "." segítségével az adott valami metódusait vagy
		// adattagjaihoz férünk hozzá
		allatA.speak();

		// Objektum szöveges reprezentációjának kiíratása
		System.out.println(allatA);
		System.out.println(allatB);

		// Két objektum csak akkor egyenlő az "==" operátor szerint
		// ha ténylegesen ugyanazt hasonlítjuk össze
		// tehár allatA == allatA az true, de ez már false:
		if(allatA == allatB) {
			System.out.println("Ugyanaz");
		} else {
			System.out.println("Nem ugyanaz");	// Ezt fogja kiírni
		}
	}
}