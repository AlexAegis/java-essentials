package animals.feline;

import animals.Animal;

public class Cat extends Animal {

	// a final kulcsszó konstansá teszi a mezőt/adattagot
	// értéke inicializálás után már nem változtatható
	// inicializálni lehet itt helyben
	public final boolean talpraEsik /*= true*/;

	{
		// Vagy egy ilyen inicializálómezőben is lehet
		// Ez az objektum elkészülésekor, a konstruktor előtt fut le
		talpraEsik = true;
	}

	//Konstruktor overload, a három konstruktor megkülönböztethető
	public Cat(String name) {
		this.name = name;
	}

	public Cat(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public Cat(int age) {
		this.age = age;
	}

	public void meow() {
		if(age > 10) {
			System.out.println("Cat Meoooooow!");
		} else {
			System.out.println("Cat Meow!");
		}
	}
}