A feladatokat itt a homework mappán belül külön mappákba készítsd el.
pl.: 
- homework/task01
- homework/task02
stb.

Minden feladatot miután elkészült töltsd fel GitHub-ra és ellenőrizd is le neten, hogy a változtatásaid helyesen felkerültek e a remote repository-ba.

Ne feledd, a command line-ba "cd .."-al lépsz föl egy mappát és "cd mappanév"-el lépsz be mappákba, előző parancsokat emg a fölfele/lefele gombokkal érsz el gyorsan. a "dir"-el az aktuális mappa tartalmát íratod ki a "tree" paranccsal pedig az összes mappát és annak almappáit.

Általában a main metódust tartalmazó osztály neve vagy Main vagy App (Én a Main-t preferálom)
Fordítás:
javac Main.java
Futtatás:
java Main

###Task 01

Készíts programot melyben felveszel valahova két egész számot az első legyen inicializálva 10 értékkel, a második pedig 11-el.
Írj két if() kifejezést mindkét számra ami eldönti, hogy az adott szám páros e, ha pedig az írjon ki valamit.
(Tipp: % a maradékos osztás)

###Task 02

A task03 mappában lévő Main.java-t kell kijavítanod úgy, hogy leforduljon.
(Tipp: figyeld mit üzen a fordító)

###Task 03

Készíts egy programot ami kiírja egytől tízig a számokat
A main metódusod csak 1 sort tartalmazhat.
(Tipp: Statikus környezetből -  pl a main függvény - csak statikus dolgokat érsz el, használj ciklust!)

```java
for(int i = 0; i < 10; i++) {
	//ciklusmag
}
```

###Task 04

Vegyünk fel egy String-et a main függvényünkben és legyen az értéke "helloworld" majd írjuk ki a konzolra úgy, hogy csupa nagybetű legyen.
(Tipp: Minden Stringnek van egy olyan metódusa, hogy toUpperCase() és van egy olyan párja is, hogy toLowerCase())

Majd írjuk ki ugyanezt a String-et megint úgy, hogy csupa kisbetű legyen, de mégis legyen rajta egyszer meghívva a "toUpperCase" metódus.

A main metódus csak 3 sort tartalmazhat, és ebből az első a String deklarációja legyen.

###Task 05

Készíts egy programot mely 1-100 ig kiírja az összes páratlan számot. Elég csak a Main metódusba dolgozni.
A main metódust tartalmazó osztályod legyen egy "paratlan" nevű csomagon belül.
A kódba valahova egy kommentbe írd bele, hogy hogyan fordítottad és hogyan futtattad a programot.