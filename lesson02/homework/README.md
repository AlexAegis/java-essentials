Tipp: Ha szeretnél ékezetes karaktereket látni a konzolban (mert a java alaból UTF-8 as táblát használ ezért ő jól kezeli az ékezetes karaktereket, de a Windows Command Line csak Latin II-es kódtáblát használ, ezért nem jelennek meg ezek a karakterek) akkor írd be a console-ba, hogy: "chcp 65001"
Ha nem akarod többet beírni (ez a változtatás értelemszerűen csak az aktuális Command Line-ra vonatozik) akkor a futtatás ablakba (Win+R) írd be, hogy regedit, itt navigálj el a baloldalon a "HKEY_CURRENT_USER\Software\Microsoft\Command Processor" helyre, és ebben a mappában, jobb oldalt látni fogsz kulcsokat, alájuk jobb-kattintva pedig létre tudsz hozni egy új String típusú kulcsot, a neve legyen "Autorun" ha elkészült duplaklikkelve rá tudunk neki értéket adni, az értéke pedig legyen az előző command azaz "chcp 65001"
Ezután mindig ha új command line-t nyitsz már a bővebb karaktertáblával fog indulni
Onnan tudod, hogy jól csináltad, hogy ha legközelebb elindítasz egy új cmd-t akkor az első sor az lesz, hogy "Active code page: 65001".


- Task 0

	- Nézd meg az órai anyagot, teleírtam kommentekkel és futtasd is le, tanulmányozd át.
	- A feladatokban nyugodtan készíthetsz több osztályt mint ami le van írva, lehet hogy úgy könyebb is lesz

- Task 1

	(Azt, hogy angolul vagy magyarul írod a változókat/osztályokat
	most nem érdekel, csak ékezeteket ne használj)

	- Készíts el egy pizzázónak a modelljét, legyenek alkalmazottak, az alkalmazottakan belül pedig szakácsok, futárok, takarítók, pincérek.
	- A futárokon belül pedig legyenek motorosfutárok és autósfutárok is
	- legyen mindenki egy alkalmazottak csomagon belül és azon belül is legyen a futároknak külön csomag is de maga a futár osztály ne ebben legyen csak a két speciális futár
	- Minden alkalmazottnak legyen neve, életkora és fizetése, ezeket az értékeket a konstruktoron keresztül kapják meg, ezeket a mezőket a main függvényből ne lehessen elérni!
	- a futároknak legyen egy kiszállít() metódusa is ami csak annyit csináljon, hogy kiírja, hogy meg lett hívva és, hogy hol lett meghívva (írd ki mellé a nevét is az aktuális futárnak, hogy látni lehessen) valamint írja ki utána a fizetését is.
	- Legyen az alkalmazottaknak egy fizetésemelés() metódusa is, ami 1000-et hozzáad a fizetésükhöz.
	- A motoros futár fizetésemeléskor 1500-at kapjon
	- Az autós futár fizetésemeléskor csak 500-at kapjon
	- Az utóbbi két feladatot kétféle képpen is meg lehet oldani, egyiket így másikat úgy oldd meg! (Tipp: órai anyag, equals metódusok körül)
	- A main függvényt tartalmazó Osztály pedig egy pizzéria nevű csomagban legyen (az alkalmazottak csomag is ezen belül legyen)
	- A main függvényedbe készíts mindkét fajtából egy-egy futárt és hívd meg a kiszállít függvényüket. Majd használd mindkettejükön a fizetésemelés metódust és aztán megint egy-egy kiszállítást.


- Task 2

	- Egészítsük ki az órai feladatot (csak az animals csomagot másold át, új Main-ünk lesz)
	- Lesznek madaraink és halaink is a saját csomagjukban, legyenek Grouper, Snapper halaink és Eagle és Vulture madaraink
	- Minden állatnak legyen egy konstans kedvenc étele (Egy sima String favouriteFood)
	- Minden állatnak más legyen a kedvenc étele de csak 1 kedvenc étel mezőt használhatsz
	- Minden állatnak legyen egy hatótávja is (int range) ami alapból 10, a run() metódus írja ki ennek az értékét is.
	- Minden állat tudjon enni (írja ki az állat nevét és hogy mi a kedvenc étele) és ha ettek akkor a hatótávjuk duplázódjon meg, ha viszont madarak esznek akkor triplázódjon meg. Hogy lehet ezt úgy megcsinálni, hogy a madarak miatt csak egyszer kelljen átírni az evés metódust?
	- A madarak run() metódusa (amit az Animal-ból örököltek) ne azt írja ki, hogy "Animal runs", hanem, hogy "Bird flies"
	- A halak run() metódusa pedig, hogy "Fish swims"
	- Kérdés: Kell e az equals metódusokat kiegészíteni a kedvenc étellel? Van e értelme?
	- Készíts egy Main függvényt amiben elkészítel egy tetszőleges halat és egy tetszőleges madarat majd mindkettőt megfuttatod aztán mindkettőt megeteted majd ismét megfuttatod őket.