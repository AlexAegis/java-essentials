import animals.canine.Dog;
import animals.Animal;
import animals.feline.Cat;

class Main {

	public static void main(String[] args) {

		Dog kutyaA = new Dog("Bodri");
		Dog kutyaB = new Dog("Bodri");
		Dog kutyaC = new Dog("Béla");

		System.out.println(kutyaA.name);
		// A kutya tud ugatni mert Dog és tud futni is mert Animal
		kutyaA.bark();
		kutyaA.run();

		// Dog szűk definíció
		// Animal tág definíció
		// aliasing
		Animal kutyaAMintAnimal = kutyaA;

		// A kutyaA most csak másképp van referálva 
		// de attól még ugyanúgy Dog típusú, elérjük 
		// a Dog és Animal metódusait is
		kutyaAMintAnimal.run();
		//kutyaAMintAnimal.bark();

		System.out.println("kutyaA: " + kutyaA);
		System.out.println("kutyaAMintAnimal: " + kutyaAMintAnimal);

		if(kutyaA == kutyaAMintAnimal) {
			System.out.println("KutyaA == kutyaAMintAnimal");
		}

		// Castolás
		Animal kutyaBMintAnimal = (Animal) kutyaB;

		// A kutyaB már Animal típusú, ezért csak az Animal metódusait tudjuk használni
		kutyaBMintAnimal.run();
		// kutyaBMintAnimal.bark(); most már Animal, az állatok 
		// meg nem feltétlen tudnak ugatni
		System.out.println();
		System.out.println();
		System.out.println("A kutyaB és annak átcastolt változata valójában ugyanarra az objektumra mutatnak:");
		System.out.println("kutyaB: " + kutyaB);
		System.out.println("kutyaBMintAnimal: " + kutyaBMintAnimal);
		System.out.println("Ezért egyenlőek is:");
		System.out.print("KutyaB == kutyaBMintAnimal: ");
		System.out.println(kutyaB == kutyaBMintAnimal);

		System.out.println();
		System.out.println("A kutyaA és kutyaB két különböző objektumok, akkor is ha belülről a mezőik ugyanazokat az értékeket tartalmazzák: ");
		System.out.println("kutyaA: " + kutyaA);
		System.out.println("kutyaB: " + kutyaB);
		System.out.println("kutyaA name: " + kutyaA.name);
		System.out.println("kutyaB name: " + kutyaB.name);
		System.out.println("kutyaA age: " + kutyaA.getAge());
		System.out.println("kutyaB age: " + kutyaB.getAge());
		System.out.println("Ezért az \"==\" operátorral összehasonlítva őket hamisat is fogunk kapni");
		System.out.print("KutyaA == kutyaB: ");
		System.out.println(kutyaA == kutyaB);
		System.out.println("De ha írunk egy equals függvényt amiben leírjuk, hogy hogyan hasonlítsunk össze két kutyát akkor már meg tudjuk vizsgálni, hogy két objektum belső szerkezete megegyezik e");
		System.out.print("KutyaA.equals(kutyaB): ");
		System.out.println(kutyaA.equals(kutyaB));


		Cat macskaA = new Cat("Bodri");
		Cat macskaB = new Cat("Bodri", 0);
		Cat macskaC = new Cat(20);

		System.out.println();
		System.out.println("KutyaA és macskaA belső szerkezete hiába 'egyezik' meg, azaz hiába ugyanaz a nevük és koruk, mivel más típusúak ezért nem lehetnek egyenlőek");
		System.out.println("kutyaA: " + kutyaA);
		System.out.println("macskaA: " + macskaA);
		System.out.println("kutyaA name: " + kutyaA.name);
		System.out.println("macskaA name: " + macskaA.name);
		System.out.println("kutyaA age: " + kutyaA.getAge());
		System.out.println("macskaA age: " + macskaA.getAge());
		System.out.print("KutyaA.equals(macskaA): ");
		System.out.println(kutyaA.equals(macskaA));

		Animal macskaBmintAnimal = (Animal) macskaB;

		System.out.println();
		System.out.println("De ha már Animalként kezeljük akkor más a helyzet");
		System.out.print("macskaBmintAnimal.equals(kutyaBMintAnimal): ");
		System.out.println(macskaBmintAnimal.equals(kutyaBMintAnimal));
	}
}